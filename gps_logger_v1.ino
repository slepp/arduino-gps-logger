//#include <Wire.h>
//#include <SoftwareSerial.h>
//#include <digitalWriteFast.h>
//#include <progmem.h>
//#include <AltSoftSerial.h>

#include <EEPROM.h>

#include "PinChangeInt.h"

#include <SPI.h>
#include <SD.h>

#include "ubx_defines.h"

//SoftwareSerial btSerial(5,9);
//AltSoftSerial btSerial;
#define btSerial Serial

#define MAX_PAYLOAD (sizeof(ubx)-6)

#define UPD_CRC calc_ck_a += c; calc_ck_b += calc_ck_a;
byte ubloxDecode(unsigned char c) {
  static enum { S_SYNC, S_SYNC2, S_CLASS, S_ID, S_LENGTH, S_LENGTH2, S_PAYLOAD, S_CK_A, S_CK_B } state = S_SYNC;
  static unsigned char calc_ck_a, calc_ck_b;
  static unsigned char payload[MAX_PAYLOAD];
  static unsigned short bpos = 0;
  byte retval = 0;
  switch (state) {
      // Idle / wait for sync byte #1
    case S_SYNC:
      if (c == 0xB5) {
        state = S_SYNC2;
      }
      break;
      // Expect second sync byte, loop if we get #1 again
    case S_SYNC2:
      if (c == 0x62)
        state = S_CLASS;
      else if (c == 0xB5)
        state = S_SYNC2; // Stay in this mode if we get sync1 again
      else
        state = S_SYNC;
      calc_ck_a = 0; calc_ck_b = 0;
      break;
      // UBX Class identifier
    case S_CLASS:
      ubx.cls = c; state = S_ID; UPD_CRC; break;
      // UBX ID
    case S_ID:
      ubx.id = c; state = S_LENGTH; UPD_CRC; break;
      // Low byte length header
    case S_LENGTH:
      ubx.len = c;
      state = S_LENGTH2;
      UPD_CRC;
      break;
      // High byte length header
    case S_LENGTH2:
      ubx.len += c << 8;
      if (ubx.len == 0)
        state = S_CK_A;
      else
        state = S_PAYLOAD;
      bpos = 0;
      UPD_CRC;
      break;
    case S_PAYLOAD: // byte counter
      payload[bpos++] = c;
      //*(((uint8_t *)&ubx.nav)+bpos++) = c; // Direct into the packet buffer
      if (bpos == MAX_PAYLOAD)
        state = S_SYNC; // reset on overflow
      UPD_CRC;
      if (bpos == ubx.len)
        state = S_CK_A;
      break;
    case S_CK_A:
      ubx.ck_a = c; state = S_CK_B;
      if (ubx.ck_a != calc_ck_a) {
        state = S_SYNC; // Cancel packet
      }
      break;
    case S_CK_B:
      ubx.ck_b = c; state = S_SYNC;
      if (ubx.ck_b != calc_ck_b) {
        break;
      }
      // Checksum passed, clone in the payload
      // Copy into the 'nav' area, which shares all the packet formats
      // Only copy in maximum size of the ubx struct, minus headers,
      // or the actual payload size, whichever is smaller.
      memcpy(&ubx.nav, payload, (sizeof(ubx) - 6 < ubx.len ? sizeof(ubx) - 6 : ubx.len));
      retval = 1;
      break;
  }
  return retval;
}

#define TWIWait while((TWCR & (1<<TWINT)) == 0)

inline void TWIStart() {
  TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
  TWIWait;
}

inline void TWIRestart() {
  TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWSTO) | (1<<TWEN);
  TWIWait;
}

inline void TWIWrite(uint8_t data) {
  TWDR = data;
  TWCR = (1<<TWINT)|(1<<TWEN);
  TWIWait;
}

inline uint8_t TWIRead(bool last) {
  if(last)
    TWCR = (1<<TWINT)|(1<<TWEN);
  else
    TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
  TWIWait;
  return TWDR;
}

inline void TWIWrite_NoWait(uint8_t data) { // Don't busy loop waiting
  TWDR = data;
  TWCR = (1<<TWINT)|(1<<TWEN);
}

inline void TWIStop() {
  TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN);
  while (TWCR & (1<<TWSTO)); // Stop only clears the control register bit
}

// Send the character, and calculate the checksum while it transmits
#define TWI_UPD_CRC(c) TWIWrite_NoWait((c)); ck_a += (uint8_t)(c); ck_b += ck_a; TWIWait

void sendFrame(uint8_t cls, uint8_t id, uint16_t len, const char *data) {
  uint16_t dpos = 0;
  uint8_t ck_a = 0;
  uint8_t ck_b = 0;
  uint8_t *ulen = (uint8_t *)&len; // Map the 16 bits to two 8 bits
  
  TWIStart();
  TWIWrite(0x42<<1);
  TWIWrite(0xB5);
  TWIWrite(0x62);
  TWI_UPD_CRC(cls);
  TWI_UPD_CRC(id);
  TWI_UPD_CRC(ulen[0]);
  TWI_UPD_CRC(ulen[1]);
  for(dpos = 0; dpos < len; ++dpos) {
    TWI_UPD_CRC(data[dpos]);
  }
  TWIWrite(ck_a);
  TWIWrite(ck_b);
  TWIStop();
}

void sendFrame(uint8_t cls, uint8_t id, uint16_t len, const __FlashStringHelper *data) {
  uint16_t dpos = 0;
  uint8_t ck_a = 0;
  uint8_t ck_b = 0;
  uint8_t *ulen = (uint8_t *)&len; // Map the 16 bits to two 8 bits
  
  TWIStart();
  TWIWrite(0x42<<1);
  TWIWrite(0xB5);
  TWIWrite(0x62);
  TWI_UPD_CRC(cls);
  TWI_UPD_CRC(id);
  TWI_UPD_CRC(ulen[0]);
  TWIWrite_NoWait(ulen[1]);
  ck_a += (uint8_t)ulen[1];
  ck_b += ck_a;
  for(dpos = 0; dpos < len; ++dpos) {
    unsigned char c;
    // This will fetch while the previous transmits
    c = pgm_read_byte_near((prog_uchar *)data + dpos);
    TWIWait; // Wait for previous completion
    TWIWrite_NoWait(c); // Send character
    ck_a += (uint8_t)c;
    ck_b += ck_a;
  }
  TWIWait;
  TWIWrite(ck_a);
  TWIWrite(ck_b);
  TWIStop();
}

inline void messageType(uint8_t cls, uint8_t id, uint8_t on) {
  char buf[8] = { cls, id, on, 0, 0, 0, 0, 0 };
  sendFrame(0x06, 0x01, 0x08, buf);
}

File dataFile;

bool loggingEnabled = true;

volatile bool txReady = true;
void txReadyInterrupt() {
  txReady = true;
}

const byte ledIO = A0;
const byte ledLogging = A3;
const byte ledTracking = A7;

void setup() {
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(A6, OUTPUT);
  pinMode(A7, OUTPUT);
  pinMode(4, INPUT); // txReady input
  digitalWrite(ledIO, HIGH);
  digitalWrite(A1, LOW); // GND for ledIO
  digitalWrite(A2, LOW); // GND for ledLogging
  digitalWrite(ledLogging, LOW);
  digitalWrite(A6, LOW); // GND for ledTracking
  digitalWrite(ledTracking, LOW);
  
  // D7 will be used to enable/disable logging
  pinMode(7, INPUT_PULLUP);
  //digitalWrite(7, HIGH);

  //Serial.begin(38400);
  btSerial.begin(57600);
  //gpsSerial.begin(38400); // For uploading data to the GPS
  
  //Wire.begin();
  TWSR = 0; // No prescaler
  TWBR = 10; // Bit rate divider to get 444kHz
  TWCR = (1<<TWEN) | // Enable
         (0<<TWIE) | (0<<TWINT) | // No interrupts
         (0<<TWEA) | (0<<TWSTA) | (0<<TWSTO) | // No state
         (0<<TWWC); // Collision clear
  
  //btSerial.print("AT+NAMEGPS1\r\n"); // Name the serial Bluetooth
  //delay(1100);
  delay(500);
  
  //Serial.println(F("Initial configuration")); // 96 00 00 07
  // UART1 - Off
  sendFrame(UBX_CFG, UBX_CFG_PRT, 20, F("\x01\x00\x00\x00\xD0\x08\x00\x00\x00\x96\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"));
  // I2C - txReady output on PIO06 (TXD)
  sendFrame(UBX_CFG, UBX_CFG_PRT, 20, F("\x00\x00\x99\x00\x84\x00\x00\x00\x00\x00\x00\x00\x07\x00\x03\x00\x00\x00\x00\x00"));
  // Portable mode
  sendFrame(UBX_CFG, UBX_CFG_NAV5, 0x24, F("\xFF\xFF\x00\x03\x00\x00\x00\x00\x10\x27\x00\x00\x05\x00\xFA\x00\xFA\x00\x64\x00\x2C\x01\x00\x3C\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x00\x00"));
  // Enable autonomous aiding
  sendFrame(UBX_CFG, UBX_CFG_NAVX5, 0x28, F("\x00\x00\xFF\xFF\x0F\x00\x00\x00\x03\x02\x03\x16\x07\x00\x00\x01\x00\x00\x9B\x06\x00\x00\x00\x00\x01\x01\x00\x01\x00\x64\x64\x00\x00\x01\x10\x00\x00\x00\x00\x00"));
  sendFrame(UBX_CFG, UBX_CFG_GNSS, 0x24, F("\x00\x16\x16\x04\x00\x04\xFF\x00\x01\x00\x00\x00\x01\x01\x03\x00\x01\x00\x00\x00\x05\x00\x03\x00\x01\x00\x00\x00\x06\x08\xFF\x00\x00\x00\x00\x00"));
  sendFrame(UBX_CFG, UBX_CFG_SBAS, 0x08, F("\x01\x07\x03\x00\x04\xE0\x04\x00"));
  
  //Serial.println(F("Message configuration"));
  // We want to see the following messages
  messageType(UBX_NAV, UBX_NAV_AOPSTATUS, 0);
  messageType(UBX_NAV, UBX_NAV_CLOCK, 0);
  messageType(UBX_NAV, UBX_NAV_DGPS, 0);
  messageType(UBX_NAV, UBX_NAV_DOP, 0);
  messageType(UBX_NAV, UBX_NAV_POSECEF, 0);
  messageType(UBX_NAV, UBX_NAV_POSLLH, 0);
  messageType(UBX_NAV, UBX_NAV_PVT, 1); // Largest chunk of info in one frame
  messageType(UBX_NAV, UBX_NAV_SBAS, 0);
  messageType(UBX_NAV, UBX_NAV_SOL, 0);
  messageType(UBX_NAV, UBX_NAV_STATUS, 0); // General status information
  messageType(UBX_NAV, UBX_NAV_SVINFO, 0);
  messageType(UBX_NAV, UBX_NAV_TIMEGPS, 0);
  messageType(UBX_NAV, UBX_NAV_TIMEUTC, 0); // Tick-tock
  messageType(UBX_NAV, UBX_NAV_VELECEF, 0);
  messageType(UBX_NAV, UBX_NAV_VELNED, 0);
  messageType(UBX_AID, UBX_AID_REQ, 1);
  messageType(UBX_AID, UBX_AID_ALPSRV, 1);
  messageType(UBX_AID, UBX_AID_AOP, 1);
  messageType(UBX_AID, UBX_AID_ALM, 1);
  messageType(UBX_AID, UBX_AID_EPH, 1);
  
  // Disable all the other NMEA payloads
  //Serial.println(F("Disabling NMEA"));
  messageType(0xF0, 0x0A, 0);
  messageType(0xF0, 0x09, 0);
  messageType(0xF0, 0x00, 0);
  messageType(0xF0, 0x01, 0);
  messageType(0xF0, 0x43, 0);
  messageType(0xF0, 0x42, 0);
  messageType(0xF0, 0x0D, 0);
  messageType(0xF0, 0x40, 0);
  messageType(0xF0, 0x06, 0);
  messageType(0xF0, 0x02, 0);
  messageType(0xF0, 0x07, 0);
  messageType(0xF0, 0x03, 0);
  messageType(0xF0, 0x04, 0);
  messageType(0xF0, 0x41, 0);
  messageType(0xF0, 0x05, 0);
  messageType(0xF0, 0x08, 0);

  // CFG-RATE
  //Serial.println(F("Rate configuration"));
  sendFrame(UBX_CFG, UBX_CFG_RATE, 6, F("\xE8\x03\x01\x00\x01\x00"));
  
  // Fire up the SD
  //Serial.println(F("SD initialization"));
  pinMode(10, OUTPUT);
  while(!SD.begin(10)) {
    //Serial.println(F("Card failed."));
    delay(100);
  }
  
  //Serial.println(F("Started"));

  digitalWrite(ledIO, LOW); // "Busy" LED
  digitalWrite(ledTracking, LOW); // Tracking OK LED
  digitalWrite(ledLogging, (loggingEnabled?HIGH:LOW)); // Logging On LED
  
  attachInterrupt(1, txReadyInterrupt, RISING);
  //PCintPort::attachInterrupt(4, txReadyInterrupt, RISING);
}

#define HUI_EE_OFFSET 0
void sendHui() {
  char hui[72];
  uint8_t x, b;
  if(EEPROM.read(HUI_EE_OFFSET + 72) != 0x9A)
    return;
  for(x = HUI_EE_OFFSET, b = 0; b < 72; x++, b++) {
    hui[b] = EEPROM.read(x);
  }
  sendFrame(UBX_AID, UBX_AID_HUI, 72, hui);
}

// Write the HUI data to EEPROM if it varies
void writeHui() {
  uint8_t x, b;
  for(x = HUI_EE_OFFSET, b = 0; b < 72; x++, b++) {
    if(EEPROM.read(x) != ubx.aid.hui.payload[b]) {
      EEPROM.write(x, ubx.aid.hui.payload[b]);
    }
  }
  if(EEPROM.read(HUI_EE_OFFSET+72) != 0x9A) {
    EEPROM.write(HUI_EE_OFFSET+72, 0x9A); // "HUI valid" flag
  }
  if(SD.exists("GPSHUI.UBX"))
    SD.remove("GPSHUI.UBX");
  dataFile = SD.open("GPSHUI.UBX", FILE_WRITE);
  dataFile.write((const uint8_t *)&ubx.aid.hui, sizeof(ubx.aid.hui));
  dataFile.close();
}

#define ALM_EE_OFFSET 74
void sendAlm() {
  char alm[40];
  uint8_t x, b;
  if((ALM_EE_OFFSET+(ubx.aid.alm.svid*36)) > (1024-36))
    return;
  *((uint32_t *)alm) = ubx.aid.alm.svid;
  for(x = ALM_EE_OFFSET + (ubx.aid.alm.svid*36), b = 4; b < ubx.len; x++, b++) {
    alm[b] = EEPROM.read(x);
  }
  sendFrame(UBX_AID, UBX_AID_ALM, 40, alm);
}
// Store almanac bodies of up to 36 bytes
void writeAlm() {
  char fname[12];
  uint8_t x, b;
  snprintf_P(fname, 12, PSTR("ALM-%X.UBX"), ubx.aid.alm.svid&0xFF);
  if(SD.exists(fname))
    SD.remove(fname);
  dataFile = SD.open(fname, FILE_WRITE);
  dataFile.write((const uint8_t *)&ubx.aid.alm, ubx.len);
  dataFile.close();
  dataFile = SD.open("ALM.UBX", FILE_WRITE);
  dataFile.seek(sizeof(ubx.aid.alm) * ubx.aid.alm.svid);
  dataFile.write((const uint8_t *)&ubx.aid.alm, ubx.len);
  dataFile.close();
  if((ALM_EE_OFFSET+(ubx.aid.alm.svid*36)) > (1024-36))
    return;
  for(x = ALM_EE_OFFSET + (ubx.aid.alm.svid*36), b = 4; b < ubx.len; x++, b++) {
    if(EEPROM.read(x) != ubx.aid.alm.payloadRaw[b]) {
      EEPROM.write(x, ubx.aid.alm.payloadRaw[b]);
    }
  }
}

void writeAOP() {
  char fname[12];
  uint8_t x, b;
  snprintf_P(fname, 12, PSTR("AOP-%X.UBX"), ubx.aid.aop.svid);
  if(SD.exists(fname))
    SD.remove(fname);
  dataFile = SD.open(fname, FILE_WRITE);
  dataFile.write((const uint8_t *)&ubx.aid.aop, ubx.len);
  dataFile.close();
  dataFile = SD.open("AOP.UBX", FILE_WRITE);
  dataFile.seek(sizeof(ubx.aid.aop) * ubx.aid.aop.svid);
  dataFile.write((const uint8_t *)&ubx.aid.aop, ubx.len);
  dataFile.close();
}

void writeEph() {
  char fname[12];
  uint8_t x, b;
  snprintf_P(fname, 12, PSTR("EPH-%X.UBX"), ubx.aid.eph.svid&0xFF);
  if(SD.exists(fname))
    SD.remove(fname);
  dataFile = SD.open(fname, FILE_WRITE);
  dataFile.write((const uint8_t *)&ubx.aid.eph, ubx.len);
  dataFile.close();
  dataFile = SD.open("EPH.UBX", FILE_WRITE);
  dataFile.seek(sizeof(ubx.aid.eph) * ubx.aid.eph.svid);
  dataFile.write((const uint8_t *)&ubx.aid.eph, ubx.len);
  dataFile.close();
}
// In case we miss an interrupt or such, always check after an interval
uint8_t idleCounter = 0;
enum { SEND_NONE, SEND_HUI, SEND_INI, SEND_DATA, SEND_TRACK_1HZ, SEND_TRACK_4HZ, SEND_TRACK_10HZ } sendData = SEND_NONE; // Send data out to the receiver at its request
enum { TRACK_RATE_1HZ, TRACK_RATE_4HZ, TRACK_RATE_10HZ } trackingRate = TRACK_RATE_1HZ;

void sendAlm(uint8_t svid) {
  uint8_t ck_a = 0, ck_b = 0;
  uint8_t len = 40;
  if(!SD.exists("ALM.UBX"))
    return;
  dataFile = SD.open("ALM.UBX", FILE_READ);
  dataFile.seek(svid * sizeof(ubx.aid.alm));
  TWIStart();
  TWIWrite(0x42);
  TWIWrite(0xB5);
  TWIWrite(0x62);
  TWIWrite(UBX_AID); ck_a = UBX_AID; ck_b = ck_a;
  TWIWrite(UBX_AID_ALM); ck_a += UBX_AID_ALM; ck_b += ck_a;
  TWIWrite(len); ck_a += len; ck_b += ck_a;
  TWIWrite_NoWait(0); ck_a += 0; ck_b += ck_a;
  do {
    uint8_t c = dataFile.read();
    TWIWait;
    TWIWrite_NoWait(c); ck_a += c; ck_b += ck_a;
  } while(len-- && dataFile.available());
  TWIWait;
  TWIWrite(ck_a);
  TWIWrite(ck_b);
  TWIStop();
  dataFile.close();
}

void handleAiding() {
  // Payloads less than 2 bytes are poll packets
  if(ubx.cls == UBX_AID && ubx.id == UBX_AID_AOP && ubx.len > 1) {
    writeAOP();
  }
  // Ephemeris data, will be only 8 bytes if empty
  if(ubx.cls == UBX_AID && ubx.id == UBX_AID_EPH && ubx.len > 8) {
    writeEph();
  }
  // Almanac data, will be only 8 bytes if empty
  if(ubx.cls == UBX_AID && ubx.id == UBX_AID_ALM && ubx.len > 8) {
    writeAlm();
  }
  // HUI response, will always be 72 bytes when full of data
  if(ubx.cls == UBX_AID && ubx.id == UBX_AID_HUI && ubx.len == 72) {
    writeHui();
  }
  if(ubx.cls == UBX_AID && ubx.id == UBX_AID_ALM && ubx.len == 1) {
    // Send almanac?
  }
  if(ubx.cls == UBX_AID && ubx.id == UBX_AID_EPH && ubx.len == 1) {
    // Send ephemeris?
  }
  // Send initial data set of time/position/etc.
  if(ubx.cls == UBX_AID && ubx.id == UBX_AID_INI && ubx.len == 0) {
    sendData = SEND_INI;
  }
  // Request to send the HUI parameters
  if(ubx.cls == UBX_AID && ubx.id == UBX_AID_HUI && ubx.len == 0) {
    sendData = SEND_HUI;
  }
  // Startup request for all aiding data
  if(ubx.cls == UBX_AID && ubx.id == UBX_AID_DATA && ubx.len == 0) {
    sendData = SEND_DATA;
  }
}

void loop() {
  uint8_t wireBuf[32];
  uint8_t wireBufPos = 0;
  uint16_t len;

  // If the txReady pin was brought up
  if(txReady || ++idleCounter > 100) {
    idleCounter = 0;
    // Read length of I2C buffers
    TWIStart();
    TWIWrite(0x42<<1);
    TWIWrite(0xFD);
    TWIStart();
    TWIWrite(0x42<<1|1);
    len = TWIRead(false) << 8;
    len += TWIRead(true);
    TWIStop();
    cli(); // Block interrupts while we check the length as 0 and reset txReady
    if(len == 0)
      txReady = false; // No more data
    sei();
    if(len > 0)
      delayMicroseconds(30);
  } else {
    // No ready indication, so nothing to read
    len = 0;
  }
  
  if (len >= 8) { // Wait for at least 8 bytes, which is the minimum frame size
    //do {
      uint8_t toRead = (len > 32 ? 32 : len);
      fastDigitalWrite(ledIO, HIGH);
      TWIStart();
      TWIWrite(0x42<<1 | 1);
      wireBufPos = 0;
      do {
        char c = TWIRead((toRead == 1 ? true : false));
        --len;
        wireBuf[wireBufPos++] = c;
        if (ubloxDecode(c)) { // Frame!
          // Only log valid fix frames
          digitalWrite(ledLogging, HIGH);
          if (ubx.cls == UBX_NAV && ubx.id == UBX_NAV_PVT) {
            if(!ubx.nav.pvt.flags.gnssFixOK) {
              digitalWrite(ledTracking, LOW);
              if(trackingRate == TRACK_RATE_10HZ)
                sendData = SEND_TRACK_1HZ;
            } else { // Valid, log it
              digitalWrite(ledTracking, HIGH);
              if(trackingRate == TRACK_RATE_1HZ)
                sendData = SEND_TRACK_10HZ;
              //if(loggingEnabled) {
                dataFile = SD.open("gpsraw.ubx", FILE_WRITE);
                dataFile.write((const uint8_t *)"\xB5\x62", 2);
                dataFile.write((const uint8_t *)&ubx, 4);
                dataFile.write((const uint8_t *)&ubx.nav.pvt, sizeof(ubx.nav.pvt));
                dataFile.write((const uint8_t *)&ubx.ck_a, 2);
                dataFile.close();
              //}
            }
          }
          digitalWrite(ledLogging, LOW);
        }
        if(ubx.cls == UBX_AID) {
          handleAiding();
        }
      } while(--toRead);
      TWIStop();
      fastDigitalWrite(ledIO, LOW);
      if(wireBufPos > 0) {
        btSerial.write(wireBuf, wireBufPos);
#ifndef btSerial
        btSerial.flushOutput();
#endif
      }
    //} while(len);
  }
  
  switch(sendData) {
    case SEND_INI:
    case SEND_DATA:
    case SEND_HUI:
      /*delayMicroseconds(20);
      sendHui();
      delayMicroseconds(20);*/
      break;
    case SEND_TRACK_1HZ:
      trackingRate = TRACK_RATE_1HZ;
      delayMicroseconds(20);
      sendFrame(UBX_CFG, UBX_CFG_RATE, 6, F("\xE8\x03\x01\x00\x01\x00"));
      delayMicroseconds(20);
      break;
    case SEND_TRACK_4HZ:
      trackingRate = TRACK_RATE_4HZ;
    case SEND_TRACK_10HZ:
      trackingRate = TRACK_RATE_10HZ;
      delayMicroseconds(20); // FA == 96 for 10Hz
      sendFrame(UBX_CFG, UBX_CFG_RATE, 6, F("\xFA\x00\x01\x00\x01\x00"));
      delayMicroseconds(20);
      break;
    case SEND_NONE:
    default:
      break;
  }
  sendData = SEND_NONE;
  
  /*if(digitalRead(7) == LOW) {
    delay(200);
    if(digitalRead(7) == LOW) {
      loggingEnabled = (loggingEnabled?false:true); // Toggle logging
      digitalWrite(A7, (loggingEnabled?HIGH:LOW));
    }
  }*/
  
  btSerialToI2C();
  delayMicroseconds(50); // Keep our reading not-quite-constant
}

uint8_t btBuffer[256];
uint8_t btBufferPos = 0;
void btSerialDecode(uint8_t c) {
  static uint16_t len;
  static uint8_t ck_a = 0, ck_b = 0;
  static enum { S_SYNC, S_SYNC2, S_CLS, S_ID, S_LEN1, S_LEN2, S_PAYLOAD, S_CK_A, S_CK_B } state = S_SYNC;
  if(state != S_SYNC)
    btBuffer[btBufferPos++] = c;
  switch(state) {
    case S_SYNC:
      if(c == 0xB5) {
        state = S_SYNC2;
        btBuffer[0] = 0xB5;
        btBufferPos = 1;
      } else {
        btBufferPos = 0;
      }
      break;
    case S_SYNC2:
      if(c == 0x62)
        state = S_CLS;
      else if(c == 0xB5)
        state = S_SYNC2; // Stay here
      else
        state = S_SYNC; // Abort
      break;
    case S_CLS:
      len = 1;
      ck_a = c;
      ck_b = ck_a;
      state = S_ID;
      break;
    case S_ID:
      ck_a += c;
      ck_b += ck_a;
      state = S_LEN1;
      break;
    case S_LEN1:
      ck_a += c;
      ck_b += ck_a;
      len = c;
      state = S_LEN2;
      break;
    case S_LEN2:
      ck_a += c;
      ck_b += ck_a;
      len += c << 8;
      if(len > 0)
        state = S_PAYLOAD;
      else // Otherwise, no payload, read in checksum
        state = S_CK_A;
      if(len > 248) {
        //Serial.println(F("Frame too long"));
        state = S_SYNC; // Too long, abort
      }
      break;
    case S_PAYLOAD:
      ck_a += c;
      ck_b += ck_a;
      --len;
      if(len == 0) {
        state = S_CK_A;
      }
      break;
    case S_CK_A:
      if(ck_a != c) { // Discard on checksum mismatch
        //Serial.print(F("CK_A error: "));
        //Serial.print(c, HEX);
        //Serial.print(F(" (received) != "));
        //Serial.println(ck_a, HEX);
        state = S_SYNC;
      } else {
        state = S_CK_B;
      }
      break;
    case S_CK_B:
      if(ck_b != c) { // Discard on checksum mismatch
        //Serial.print(F("CK_B error: "));
        //Serial.print(c, HEX);
        //Serial.print(F(" (received) != "));
        //Serial.println(ck_b, HEX);
      } else {
        // Fire it off in one transaction
        TWIStart();
        TWIWrite(0x42<<1);
        for(uint8_t pos = 0; pos < btBufferPos; pos++) {
          //Serial.print(btBuffer[pos], HEX); Serial.print(' ');
          TWIWrite(btBuffer[pos]);
        }
        TWIStop();
        //Serial.print(F("I2C "));
        //Serial.println(btBufferPos);
        //if(btBufferPos[2] == UBX_AID && btBufferPos[3] == UBX_AID_ALPSRV) {
        //}
      }
      state = S_SYNC;
      break;
  }     
}  

void btSerialToI2C() {
  while(btSerial.available())
    btSerialDecode(btSerial.read());
}
